import express from "express";
import { engine } from "express-handlebars";

const app = express();
const port = 3000;

app.engine("handlebars", engine());
app.set("view engine", "handlebars");
app.set("views", "./views");

app.get("/", (req, res) => {
  res.render("home", {
    message: "How are you too?",
    people: ["John", "Jessy", "James", "Ash"],
  });
});

app.get("/book/:bookId/:bookName", (req, res) => {
  res.render("book", {
    book: {
      id: req.params.bookId,
      name: req.params.bookName,
    },
  });
});

app.listen(port, () => {
  console.log(`day2 handlebars is running at http://localhost:${port}`);
});
